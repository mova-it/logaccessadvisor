package mova.logaccessadvisor;

import mova.logaccessadvisor.injector.Injector;
import mova.logaccessadvisor.injector.LogAccessInjector;
import mova.util.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.beans.BeansException;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static mova.logaccessadvisor.LogAccessConfig.AdvisorInfo;
import static mova.logaccessadvisor.LogAccessConfig.InjectorInfo;

@Component
public class LogAccessAdvisorsRegistar implements BeanDefinitionRegistryPostProcessor, EnvironmentAware {

    private final Logger logger = LoggerFactory.getLogger(LogAccessAdvisorsRegistar.class);

    private LogAccessConfig logAccessConfig;

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        for (String advisorName : logAccessConfig.advisors.keySet()) {
            AdvisorInfo advisorInfo = logAccessConfig.advisors.get(advisorName);

            if (advisorInfo.enabled) {
                AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
                pointcut.setExpression(advisorInfo.expression);

                MutablePropertyValues mutablePropertyValues = new MutablePropertyValues();
                mutablePropertyValues.add("pointcut", pointcut);
                mutablePropertyValues.add("advice", new LogAccessAdvice(advisorName, advisorInfo.enableExecutionTimeLog));

                GenericBeanDefinition genericBeanDefinition = new GenericBeanDefinition();
                genericBeanDefinition.setBeanClass(LogAccessAdvisor.class);
                genericBeanDefinition.setPropertyValues(mutablePropertyValues);

                registry.registerBeanDefinition(advisorName, genericBeanDefinition);

                logger.debug("Registered {} with {}", advisorName, advisorInfo.expression);
            } else {
                logger.debug("Avoid registering {} with {}", advisorName, advisorInfo.expression);
            }
        }

        for (String injectorName : logAccessConfig.injectors.keySet()) {
            InjectorInfo injectorInfo = logAccessConfig.injectors.get(injectorName);

            GenericBeanDefinition genericBeanDefinition = new GenericBeanDefinition();
            genericBeanDefinition.setBeanClass(injectorInfo.type);

            registry.registerBeanDefinition(injectorName, genericBeanDefinition);
        }
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        Map<String, LogAccessAdvisor> advisors = beanFactory.getBeansOfType(LogAccessAdvisor.class);

        for (String injectorName : logAccessConfig.injectors.keySet()) {

            LogAccessInjector injector = beanFactory.getBean(injectorName, LogAccessInjector.class);
            InjectorInfo injectorInfo = logAccessConfig.injectors.get(injectorName);

            addInjector(injector, injectorInfo, advisors);
        }

        Map<String, Object> annotatedBeans = beanFactory.getBeansWithAnnotation(Injector.class);
        Map<String, LogAccessInjector> annotatedInjectors = MapUtils.mapValuesTo(annotatedBeans, LogAccessInjector.class);
        for (LogAccessInjector injector : annotatedInjectors.values()) {
            Injector injectorAnnotation = injector.getClass().getAnnotation(Injector.class);
            InjectorInfo injectorInfo = InjectorInfo.fromAnnotation(injectorAnnotation, injector.getClass());

            addInjector(injector, injectorInfo, advisors);
        }
    }

    @Override
    public void setEnvironment(Environment environment) {
        logAccessConfig = Binder.get(environment)
                .bind("logaccess", LogAccessConfig.class)
                .orElseThrow(IllegalStateException::new);
    }

    //***********************************************************************************************************************
    // PRIVATE METHODS
    //***********************************************************************************************************************

    private void addInjector(LogAccessInjector injector, InjectorInfo injectorInfo, Map<String, LogAccessAdvisor> advisors) {
        Set<String> advisorNames = new HashSet<>(advisors.keySet());
        if (!injectorInfo.includes.isEmpty()) advisorNames.retainAll(injectorInfo.includes);
        if (!injectorInfo.excludes.isEmpty()) injectorInfo.excludes.forEach(advisorNames::remove);

        for (String advisorName : advisorNames) {
            advisors.get(advisorName).getAdvice().addInjector(injector);
        }
    }

}
