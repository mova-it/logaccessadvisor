package mova.logaccessadvisor;

import org.slf4j.MDC;
import org.springframework.lang.Nullable;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class AbstractMdcInfoInterceptor extends HandlerInterceptorAdapter {

  public static final String AUTHENTICATED_USER_KEY = "AUTHENTICATED_USER";

  public abstract String getAuthenticatedUserUsername(HttpServletRequest request);

  @Override
  public boolean preHandle(HttpServletRequest request, @Nullable HttpServletResponse response, @Nullable Object handler) {
    putMdcInfo(request, getAuthenticatedUserUsername(request));

    return true;
  }

  public boolean preHandle(HttpServletRequest request) {
    return preHandle(request, null, null);
  }

  @Override
  public void afterCompletion(@Nullable HttpServletRequest request, @Nullable HttpServletResponse response, @Nullable Object handler, @Nullable Exception ex) {
    removeMdcInfo();
  }

  public void afterCompletion() {
    afterCompletion(null, null, null, null);
  }

  static void putMdcInfo(HttpServletRequest request, @Nullable String username) {
    if (username != null) {
      MDC.put(AUTHENTICATED_USER_KEY, username);
      request.setAttribute(AUTHENTICATED_USER_KEY, username);
    }
  }

  static void removeMdcInfo() {
    MDC.remove(AUTHENTICATED_USER_KEY);
  }
}
