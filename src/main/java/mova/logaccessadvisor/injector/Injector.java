package mova.logaccessadvisor.injector;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Component
public @interface Injector {

    String value();
    String[] includes() default {};
    String[] excludes() default {};
}
