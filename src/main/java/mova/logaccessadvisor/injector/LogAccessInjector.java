package mova.logaccessadvisor.injector;

public interface LogAccessInjector {

    default void injectBeforeProceed() {}
    default void injectAfterProceed(boolean hasThrowable) {}
    default void clean() {}
}
