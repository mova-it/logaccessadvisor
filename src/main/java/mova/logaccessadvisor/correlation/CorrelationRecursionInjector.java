package mova.logaccessadvisor.correlation;

import mova.logaccessadvisor.injector.Injector;
import mova.logaccessadvisor.injector.LogAccessInjector;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Injector("correlation")
@ConditionalOnExpression("${logaccess.correlation.enabled:false} && ${logaccess.correlation.recursion.enabled:false}")
public class CorrelationRecursionInjector implements LogAccessInjector {

    private static final String RECURSION_LEVEL_KEY = "RECURSION_LEVEL";

    private static final Map<String, Integer> RECURSION_PER_REQUEST = Collections.synchronizedMap(new HashMap<>());

    @Override
    public void injectBeforeProceed() {
        // Si elle existe on récupère le potentiel id de correlation qui se trouve dans ses headers ou attributs (provenant d'autres modules donc potentiellement)
        String correlationId = MDC.get(LogCorrelationInterceptor.CORRELATION_ID_KEY);

        // Si on a un id de correlation, on l'ajoute au MDC
        if (StringUtils.isNotBlank(correlationId)) {
            // On incrémente le niveau de récursion pour cet id
            RECURSION_PER_REQUEST.compute(correlationId, (key, counter) -> counter != null ? ++counter : 1);
        }
    }

    @Override
    public void injectAfterProceed(boolean hasThrowable) {
        // Si on a stocké un id de correlation (certains appels peuvent ne pas être lié à un id de correlation), on ajoute le niveau de recursion au MDC
        String correlationId = MDC.get(LogCorrelationInterceptor.CORRELATION_ID_KEY);

        // Si on a un id de correlation, on l'ajoute au MDC
        if (StringUtils.isNotBlank(correlationId)) {
            Integer recursionLevel = RECURSION_PER_REQUEST.get(correlationId);
            MDC.put(RECURSION_LEVEL_KEY, recursionLevel != null ? recursionLevel.toString() : null);
        }
    }

    @Override
    public void clean() {
        // On décrémente le niveau de log (Important: cela doit avoir lieu après l'execution de la méthode et le log d'accès)
        String correlationId = MDC.get(LogCorrelationInterceptor.CORRELATION_ID_KEY);
        RECURSION_PER_REQUEST.computeIfPresent(correlationId, (key, counter) -> counter == 1 ? null : --counter);

        // On nettoie le MDC
        MDC.remove(RECURSION_LEVEL_KEY);
    }
}
