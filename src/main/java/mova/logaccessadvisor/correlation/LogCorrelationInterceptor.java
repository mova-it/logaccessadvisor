package mova.logaccessadvisor.correlation;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.atomic.AtomicLong;

@Component
@ConditionalOnExpression("${logaccess.correlation.enabled:false}")
public class LogCorrelationInterceptor extends HandlerInterceptorAdapter {

    static final String CORRELATION_ID_KEY = "CORRELATION_ID";

    private static final AtomicLong ID_GENERATOR = new AtomicLong();

    private final Logger logger = LoggerFactory.getLogger(LogCorrelationInterceptor.class);

    private final String idPrefix;

    public LogCorrelationInterceptor(@Value("${logaccess.correlation.prefix:}") String idPrefix) {
        this.idPrefix = idPrefix;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if (logger.isDebugEnabled()) {
            String uri = request.getRequestURI();
            String[] uriParts = uri.split("/");
            String resourceParts = uriParts[uriParts.length - 1];
            if (resourceParts.contains(".")) {
                logger.warn("Cette ressource n'a pas lieu d'être traiter par l'intercepteur d'id de correlation: {}", uri);
            }
        }

        String correlationId = request.getHeader(CORRELATION_ID_KEY);
        if (StringUtils.isBlank(correlationId)) correlationId = idPrefix + ID_GENERATOR.incrementAndGet();
        MDC.put(CORRELATION_ID_KEY, correlationId);

        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        MDC.remove(CORRELATION_ID_KEY);
    }
}
