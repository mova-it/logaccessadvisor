package mova.logaccessadvisor;

import mova.logaccessadvisor.injector.LogAccessInjector;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class LogAccessAdvice implements MethodInterceptor {

    private static final String EXECUTION_TIME_KEY = "EXECUTION_TIME";
    private static final String ADVISOR_NAME_KEY = "ADVISOR_NAME";

    private final Logger logger = LoggerFactory.getLogger(LogAccessAdvice.class);

    private final String name;
    private final boolean enableExecutionTimeLog;
    private final List<LogAccessInjector> injectors = new ArrayList<>();

    LogAccessAdvice(String name, boolean enableExecutionTimeLog) {
        this.name = name;
        this.enableExecutionTimeLog = enableExecutionTimeLog;
    }

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {

        beforeInvocation();

        // On stocke le timestamp de démarrage
        long startTime = 0;
        if (enableExecutionTimeLog) startTime = System.currentTimeMillis();

        InvocationResult invocationResult = InvocationResult.invoke(invocation);

        if (enableExecutionTimeLog) {
            long endTime = System.currentTimeMillis();
            long executionTime = endTime - startTime;
            MDC.put(EXECUTION_TIME_KEY, Long.toString(executionTime));
        }

        // On renseigne le nom du composant
        MDC.put(ADVISOR_NAME_KEY, name);

        afterInvocation(invocationResult);

        try {
            return treatInvocationResult(invocation, invocationResult);
        } finally {
            cleanInvocation();
        }
    }

    private void cleanInvocation() {
        // On nettoie le MDC
        MDC.remove(EXECUTION_TIME_KEY);
        MDC.remove(ADVISOR_NAME_KEY);

        for (LogAccessInjector injector : injectors) {
            try {
                injector.clean();
            } catch (Exception e) {
                logger.error("Impossible de nettoyer les éléments injectés après l'invocation de la méthode.", e);
            }
        }
    }

    private Object treatInvocationResult(MethodInvocation invocation, InvocationResult invocationResult) throws InvocationTargetException {
        // S'il y a eu une exception, on log en erreur et on la renvoie
        if (invocationResult.hasThrowable()) {
            if (logger.isErrorEnabled()) logger.error(invocation.getStaticPart().toString(), invocationResult.getThrowable());

            invocationResult.rethrow();
        }

        // Sinon on log en info et on retourne la valeur de retour de la méthode
        if (logger.isInfoEnabled()) logger.info("{};{}", invocation.getStaticPart(), invocationResult.getResult());
        return invocationResult.getResult();
    }

    private void afterInvocation(InvocationResult invocationResult) {
        for (LogAccessInjector injector : injectors) {
            try {
                injector.injectAfterProceed(invocationResult.hasThrowable());
            } catch (Exception e) {
                logger.error("Impossible d'injecter après l'invocation de la méthode.", e);
            }
        }
    }

    private void beforeInvocation() {
        for (LogAccessInjector injector : injectors) {
            try {
                injector.injectBeforeProceed();
            } catch (Exception exception) {
                logger.error("Impossible d'injecter avant l'invocation de la méthode.", exception);
            }
        }
    }

    void addInjector(LogAccessInjector injector) {
        injectors.add(injector);
    }

}
