package mova.logaccessadvisor;

import org.aopalliance.intercept.MethodInvocation;

import java.lang.reflect.InvocationTargetException;

class InvocationResult {

    private Object result;
    private Throwable throwable;

    private InvocationResult(Object result) {
        this.result = result;
    }

    private InvocationResult(Throwable throwable) {
        this.throwable = throwable;
    }

    Object getResult() {
        return result;
    }

    Throwable getThrowable() {
        return throwable;
    }

    boolean hasThrowable() {
        return throwable != null;
    }

    void rethrow() throws InvocationTargetException {
        if (!hasThrowable()) return;

        if (throwable instanceof RuntimeException) throw (RuntimeException) throwable;
        if (throwable instanceof Error) throw (Error) throwable;

        throw new InvocationTargetException(throwable);
    }

    static InvocationResult invoke(MethodInvocation invocation) {
        try {
            // On exécute la méthode et on récupère sa valeur de retour
            return new InvocationResult(invocation.proceed());
        } catch (Throwable t) {
            // On récupère la potentielle exception ou erreur lancée par la méthode
            return new InvocationResult(t);
        }
    }
}
