package mova.logaccessadvisor;

import mova.logaccessadvisor.injector.Injector;
import mova.logaccessadvisor.injector.LogAccessInjector;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
@ConfigurationProperties("logaccess")
public class LogAccessConfig {

    /** Enum&eacute;ration des composants d&eacute;crivant les logaccessadvisors. */
    final Map<String, AdvisorInfo> advisors = new HashMap<>();
    final Map<String, InjectorInfo> injectors = new HashMap<>();

    public Map<String, AdvisorInfo> getAdvisors() {
        return advisors;
    }

    public Map<String, InjectorInfo> getInjectors() {
        return injectors;
    }

    public static class AdvisorInfo {

        String expression;
        boolean enabled = true;
        boolean enableExecutionTimeLog = true;

        public String getExpression() {
            return expression;
        }

        public void setExpression(String expression) {
            this.expression = expression;
        }

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        public boolean isEnableExecutionTimeLog() {
            return enableExecutionTimeLog;
        }

        public void setEnableExecutionTimeLog(boolean enableExecutionTimeLog) {
            this.enableExecutionTimeLog = enableExecutionTimeLog;
        }
    }

    public static class InjectorInfo {

        Class<? extends LogAccessInjector> type;
        List<String> includes = new ArrayList<>();
        List<String> excludes = new ArrayList<>();

        public Class<? extends LogAccessInjector> getType() {
            return type;
        }

        public void setType(Class<? extends LogAccessInjector> type) {
            this.type = type;
        }

        public List<String> getIncludes() {
            return includes;
        }

        public void setIncludes(List<String> includes) {
            this.includes = includes;
        }

        public List<String> getExcludes() {
            return excludes;
        }

        public void setExcludes(List<String> excludes) {
            this.excludes = excludes;
        }

        static InjectorInfo fromAnnotation(Injector injector, Class<? extends LogAccessInjector> type) {
            InjectorInfo injectorInfo = new InjectorInfo();
            injectorInfo.type = type;
            injectorInfo.includes = Arrays.asList(injector.includes());
            injectorInfo.excludes = Arrays.asList(injector.excludes());

            return injectorInfo;
        }
    }
}
