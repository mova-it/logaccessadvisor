package mova.logaccessadvisor;

import org.aopalliance.aop.Advice;
import org.springframework.aop.support.DefaultPointcutAdvisor;

public class LogAccessAdvisor extends DefaultPointcutAdvisor {

    @Override
    public void setAdvice(Advice advice) {
        if (!(advice instanceof LogAccessAdvice)) throw new IllegalArgumentException("L'advice doit être du type " + LogAccessAdvice.class + ": " + advice.getClass());

        super.setAdvice(advice);
    }

    @Override
    public LogAccessAdvice getAdvice() {
        return (LogAccessAdvice) super.getAdvice();
    }
}
